#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

int main() {
    setlocale(LC_ALL, "RUS");
    char string[101];
    cout << "Введите строку:";
    gets(string);
    char sogl[21] = "bcdfghjklmnpqrstvwxz";
    for (int j = 0; j < 100; j++)
    {
        for (int low = 0; low < 20; low++)
        {
            if (sogl[low] == string[j])
            {
                cout << sogl[low];
            }
            else
                continue;
        }
        if ((string[j] == 'a') || (string[j] == 'e') || (string[j] == 'i') || (string[j] == 'o') ||
            (string[j] == 'u') || (string[j] == 'y'))
        {
            if (string[j] == 'a')
                cout << 'A';
            else if (string[j] == 'e')
                cout << 'E';
            else if (string[j] == 'i')
                cout << 'I';
            else if (string[j] == 'o')
                cout << 'O';
            else if (string[j] == 'u')
                cout << 'U';
            else if (string[j] == 'y')
                cout << 'Y';
        }
        else if (string[j] == '\0')
            break;
    }
    return 0;
}
