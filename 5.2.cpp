#include <iostream>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
using namespace std;

//1 - находим все слова которые заканчиваются на определенную букву
//2 - находим N самых длинных слов по убыванию(с одинаковым количеством букв ставим рядом и делаем всё через массив

int main()
{
    ifstream in("input.txt");
    //слово заканчивающееся на определенную букву
    string s, temp;//строка
    getline(in,s);
    char a;
    cout<<"Введите конечную букву слова:";
    cin >> a;//конечная буква слова
    string arr;//строка со словами которые заканчиваются на определенную букву
    int p;
    cout<<endl<<"Введите количество самых длинных слов:";
    cin >> p;
    istringstream ist ( s );
    //конец головы программы
    while ( ist >> temp )
    {
        if ( *temp.rbegin() == a )
        {
            arr += temp + " ";//arr теперь имеет все слова которые заканчиваются на определенную букву(строка с этими словами)

        }
    }
    //самые длинные слова по убыванию
    // загоняем исходную строку в буфер потока
    stringstream ss (arr);

    //читаем из буфера потока отдельные слова в строковый массив
    string S[256] = {};
    int n;
    for (n = 0; ss >> S[n] ; n++);
    string mas[256];
    int k = 0;
    for (int i = 1; i <= 256; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (S[j].length() == i)
            {
                mas[k] = S[j];
                k++;
            }
        }
    }
    string ends;
    //выводим отсортированный массив
    for (int i = n-1; i > n-p-1;)
    {
        if(mas[i].size() == mas[i-1].size())
        {
            swap(mas[i],mas[i-1]);
            ends += mas[i] ;
            ends += ' ';
            ends += mas[i-1] ;
            ends += ' ';
            i -=2;
        }
        else
        {
            ends += mas[i] ;
            ends += ' ';
            i--;
        }

    }
    cout<<ends;
    ofstream out("output.txt");
    out<<ends;
    return 0;
}

